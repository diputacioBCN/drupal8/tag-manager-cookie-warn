<?php

namespace Drupal\tag_manager_cookie_warn\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Google tag manager cookie warn settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tag_manager_cookie_warn_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tag_manager_cookie_warn.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tag_manager_cookie_warn.settings');

    $form['cookie_warn'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cookie warn'),
    ];

    $languages = \Drupal::languageManager()->getLanguages();
    $texts = $config->get('texts');

    foreach ($languages as $langcode => $language) {
      $form['cookie_warn'][$langcode] = [
        '#type' => 'fieldset',
        '#title' => $language->getname(),
      ];
      $form['cookie_warn'][$langcode][$langcode . '-text'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Text'),
        '#default_value' => $texts[$langcode]['text'] ?? '',
      ];
      $form['cookie_warn'][$langcode][$langcode . '-more_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('More text'),
        '#default_value' => $texts[$langcode]['more_text'] ?? '',
      ];
      $form['cookie_warn'][$langcode][$langcode . '-more_link'] = [
        '#type' => 'textfield',
        '#title' => $this->t('More link'),
        '#default_value' => $texts[$langcode]['more_link'] ?? '',
      ];
      $form['cookie_warn'][$langcode][$langcode . '-accept_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Accept text'),
        '#default_value' => $texts[$langcode]['accept_text'] ?? '',
      ];
      $form['cookie_warn'][$langcode][$langcode . '-reject_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Reject text'),
        '#default_value' => $texts[$langcode]['reject_text'] ?? '',
      ];
    }
    $form['cookie_warn']['style'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Style'),
      '#default_value' => $config->get('style'),
      '#description' => $this->t('Extra CSS style to apply.'),
    ];
    $form['cookie_warn']['expire'] = [
      '#type' => 'number',
      '#title' => $this->t('Expire'),
      '#default_value' => $config->get('expire'),
    ];

    $form['tag_manager'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Google tag manager'),
    ];
    $form['tag_manager']['verification'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Verification site code'),
      '#default_value' => $config->get('verification'),
    ];
    $form['tag_manager']['gtm'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GTM Code'),
      '#default_value' => $config->get('gtm'),
    ];

    $options = ['denied' => $this->t('Denied'), 'granted' => $this->t('Granted')];
    $form['tag_manager']['consent_default'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Consent default'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['tag_manager']['consent_default']['default_ad_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Ad storage'),
      '#options' => $options,
      '#default_value' => $config->get('default_ad_storage'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['tag_manager']['consent_default']['default_analytics_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Analytics storage'),
      '#options' => $options,
      '#default_value' => $config->get('default_analytics_storage'),
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['tag_manager']['consent_accept'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Consent accept'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['tag_manager']['consent_accept']['accept_ad_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Ad storage'),
      '#options' => $options,
      '#default_value' => $config->get('accept_ad_storage'),
    ];
    $form['tag_manager']['consent_accept']['accept_analytics_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Analytics storage'),
      '#options' => $options,
      '#default_value' => $config->get('accept_analytics_storage'),
    ];

    $form['tag_manager']['consent_reject'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Consent reject'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['tag_manager']['consent_reject']['reject_ad_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Ad storage'),
      '#options' => $options,
      '#default_value' => $config->get('reject_ad_storage'),
    ];
    $form['tag_manager']['consent_reject']['reject_analytics_storage'] = [
      '#type' => 'select',
      '#title' => $this->t('Analytics storage'),
      '#options' => $options,
      '#default_value' => $config->get('reject_analytics_storage'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('tag_manager_cookie_warn.settings');
    $values = $form_state->getValues();

    $settings->set('style', $values['style'])
      ->set('expire', $values['expire'])
      ->set('verification', $values['verification'])
      ->set('gtm', $values['gtm'])
      ->set('default_ad_storage', $values['default_ad_storage'])
      ->set('default_analytics_storage', $values['default_analytics_storage'])
      ->set('accept_ad_storage', $values['accept_ad_storage'])
      ->set('accept_analytics_storage', $values['accept_analytics_storage'])
      ->set('reject_ad_storage', $values['reject_ad_storage'])
      ->set('reject_analytics_storage', $values['reject_analytics_storage']);

    $langcodes = array_keys(\Drupal::languageManager()->getLanguages());
    $texts = [];
    foreach ($langcodes as $langcode) {
      $texts[$langcode] = [
        'text' => $values[$langcode . '-text'],
        'more_text' => $values[$langcode . '-more_text'],
        'more_link' => $values[$langcode . '-more_link'],
        'accept_text' => $values[$langcode . '-accept_text'],
        'reject_text' => $values[$langcode . '-reject_text'],
      ];
    }
    $settings->set('texts', $texts);
    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
